<?php
defined('ABSPATH') or die("No script kiddies please!");
function whoisplug()
{
	echo '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
<head>
	<title>JQuery Form Example</title> 
	<script type="text/javascript" src="'.plugins_url( 'jquery.js' , __FILE__ ).'"></script>
	<script type="text/javascript" src="'.plugins_url( 'jqueryvalidate.js' , __FILE__ ).'"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#sorgula").validate({
			debug: false,
			rules: {
				name: "required",
				
			},
			messages: {
				name: "Please let us know who you are.",
				
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$.post("'.plugins_url( 'process.php' , __FILE__ ).'", $("#sorgula").serialize(), function(data) {
					$("#results").html(data);
				});
			}
		});
	});
	</script>
	<style>
	label.error { width: 250px; display: inline; color: red;}
	::-webkit-input-placeholder { font-style: italic; }
	:-moz-placeholder { font-style: italic; }
	::-ms-input-placeholder { font-style: italic; }
	.whoiscl{ 
		text-overflow:ellipsis;
		position:relative;
		overflow:hidden;
		
		
	}
	</style>
</head>
<body>
<form name="sorgula" id="sorgula" action="" method="POST">   
	<input size="160" type="text" name="adres" id="adres" size="30" placeholder="Başında http:// olmadan adres giriniz ve enter\'a basınız (örnek: aligoren.net)"/>  
	<br/>
	 
</form>
<div id="results"><div>
</body>
</html>
	';
}