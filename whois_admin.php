<?php
defined('ABSPATH') or die("No script kiddies please!");
/*
Plugin Name: Whois Admin Plugin
Plugin URI: http://blog.aligoren.net
Description: Whois You Admin Panel
Version: 0.1
Author: Ali GOREN
License: GPLv2
Author URI: http://blog.aligoren.net
*/



add_action('admin_menu', 'whois_button_help_tab');
function whois_button_help_tab() {
    $whois_page = add_options_page(__('Whois Admin', 'whois'), __('Whois Admin', 'whois'), 'manage_options', 'whois', 'whois_admin_page');
	add_action('load-'.$whois_page, 'whois_add_help_tab');
}

function whois_add_help_tab() {
    global $whois_page;
    $whois_screen = get_current_screen();
    
    $whois_screen->add_help_tab( array(
        'id'	=> 'whois_tab',
        'title'	=> __('Screenshot'),
        'content'	=> '
' . __( '<h3>Whois Script</h3>' ) . '
',
    ) );
	

}

function whois_admin_page() {
	require_once('whois.php');
	whoisplug();

}

?>