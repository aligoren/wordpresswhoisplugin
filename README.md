WordPressWhoisPlugin
====================

###WordPress Whois Plugin

WordPress whois sorgu çekme eklentisi. Kullanımı basit bir eklentidir.
Eklentiyi yükledikten sonra Ayarlar bölmesinde Whois Admin yazan yere tıklayıp, istediğiniz domaini girmeniz gerekiyor.
Domaini girdikten sonra enter'a basmanız yeterlidir. Eklentide jquery kullanılmıştır. Sayfa yeniden yüklenmeden eklenti size sonucu gösteriyor.
Girdiğiniz domainin başında http:// olmamasına dikkat edin.

###Download

##[Download](https://github.com/aligoren/WordPressWhoisPlugin/archive/master.zip)

###Ekran Görüntüleri:

![Ekran Görüntüsü](https://raw.githubusercontent.com/aligoren/WordPressWhoisPlugin/master/whois.png)


![Ekran Görüntüsü 2](https://raw.githubusercontent.com/aligoren/WordPressWhoisPlugin/master/whois2.png)


![Ekran Görüntüsü 3](https://raw.githubusercontent.com/aligoren/WordPressWhoisPlugin/master/whois3.png)
